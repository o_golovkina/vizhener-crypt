﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizhiner
{
    public class WordPosition
    {
        int row;
        int column;

        public WordPosition(int row, int column)
        {
            this.row = row;
            this.column = column;
        }

        public int Row { get => row; set => row = value; }
        public int Column { get => column; set => column = value; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите размер алфавита: ");
            int n = Int32.Parse(Console.ReadLine());

            char[] alphabet = new char[n];
            char[,] VizhenerTable = new char[n, n];

            Console.WriteLine("Введите символы алфавита: ");
            for (int i = 0; i < alphabet.Length; i++)
            {
                Console.Write($"[{i}]: ");
                alphabet[i] = Console.ReadLine().ToLower().ToCharArray()[0];
            }

            int shift = 0;

            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    VizhenerTable[i, j] = alphabet[(i + j) % n];

            Console.WriteLine("Полученная таблица Виженера: ");

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    Console.Write(VizhenerTable[i, j] + " ");
                Console.WriteLine();
            }

            Console.Write("Введите ключ для шифрования: ");
            string key = Console.ReadLine().ToLower();

            Console.Write("Введите сообщение, которое необходимо зашифровать: ");
            string message = Console.ReadLine().ToLower();

            key = CorrectKey(key, message);

            ShowKeyAndMessage(key, message);

            string encryptedMessage = string.Empty;

            List<WordPosition> WordsPositions = new List<WordPosition>();

            EncryptMessage(n, VizhenerTable, key, message, WordsPositions);

            foreach (var WordPosition in WordsPositions)
                encryptedMessage += VizhenerTable[WordPosition.Row, WordPosition.Column] + " ";

            encryptedMessage = encryptedMessage.TrimEnd(' ');

            Console.WriteLine("\nЗашифрованное слово: ");
            Console.WriteLine(encryptedMessage);

            encryptedMessage = encryptedMessage.Replace(" ", string.Empty);

            CorrectKey(key, encryptedMessage);

            ShowKeyAndMessage(key, encryptedMessage);

            string decodedMessage = string.Empty;

            decodedMessage = DecodeMessage(n, VizhenerTable, encryptedMessage, key, decodedMessage);

            Console.WriteLine("\nРасшифрованное слово: ");
            Console.WriteLine(decodedMessage);

            Console.Read();
        }

        private static string DecodeMessage(int n, char[,] VizhenerTable, string encryptedMessage, string newKey, string decodedMessage)
        {
            List<int> Columns = new List<int>();

            for (int j = 0; j < encryptedMessage.Length; j++)
            {
                int foundRow = 0;
                for (int i = 0; i < n; i++)
                    if (newKey[j] == VizhenerTable[i, 0])
                    {
                        foundRow = i;
                        break;
                    }
                for (int i = 0; i < n; i++)
                    if (encryptedMessage[j] == VizhenerTable[foundRow, i])
                        Columns.Add(i);
            }

            foreach (var column in Columns)
                decodedMessage += VizhenerTable[0, column] + " ";
            return decodedMessage;
        }

        private static void EncryptMessage(int n, char[,] VizhenerTable, string key, string message, List<WordPosition> WordPosition)
        {
            for (int i = 0; i < message.Length; i++)
            {
                int row = 0; int column = 0;

                for (int j = 0; j < n; j++)
                {
                    if (key[i] == VizhenerTable[j, 0])
                        row = j;
                    if (message[i] == VizhenerTable[0, j])
                        column = j;
                }
                WordPosition.Add(new WordPosition(row, column));
            }
        }

        private static string CorrectKey(string key, string message)
        {
            int count = message.Length - key.Length;

            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                    key += key[i];
            }
            else if (count < 0)
            {
                key = key.Remove(message.Length, Math.Abs(count));
            }

            return key;
        }

        private static void ShowKeyAndMessage(string key, string message)
        {
            Console.WriteLine("Ключ и сообщение: ");

            for (int i = 0; i < key.Length; i++)
                Console.Write(key[i] + " ");

            Console.WriteLine();

            for (int i = 0; i < message.Length; i++)
                Console.Write(message[i] + " ");
        }
    }
}
